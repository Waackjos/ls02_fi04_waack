import java.util.ArrayList;
import java.util.Arrays;
public class Raumschiff {
private int photonentorpedoAnzahl;
private int schildeInProzent;
private int huelleInProzent;
private int energieversorgungInProzent;
private int lebenserhaltungssystemInProzent;
private int androidenAnzahl;
private String schiffsname;
private static ArrayList<String> broadcastKommunikator = new ArrayList();
private ArrayList<Ladung> ladungsverzeichnis; 

public int getEnergieversorgungInProzent() {
	return energieversorgungInProzent;
}
public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
	this.energieversorgungInProzent = energieversorgungInProzent;
}

public int getPhotonentorpedoAnzahl() {
	return photonentorpedoAnzahl;
}
public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
}
public int getSchildeInProzent() {
	return schildeInProzent;
}
public void setSchildeInProzent(int schildeInProzent) {
	this.schildeInProzent = schildeInProzent;
}
public int getHuelleInProzent() {
	return huelleInProzent;
}
public void setHuelleInProzent(int huelleInProzent) {
	this.huelleInProzent = huelleInProzent;
}
public int getLebenserhaltungssystemInProzent() {
	return lebenserhaltungssystemInProzent;
}
public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
	this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
}
public int getAndroidenAnzahl() {
	return androidenAnzahl;
}
public void setAndroidenAnzahl(int androidenAnzahl) {
	this.androidenAnzahl = androidenAnzahl;
}
public String getSchiffsname() {
	return schiffsname;
}
public void setSchiffsname(String schiffsname) {
	this.schiffsname = schiffsname;
}
public ArrayList<String> getBroadcastKommunilator() {
	return this.broadcastKommunikator;
}
public void setBroadcastKommunilator(ArrayList<String> broadcastKommunilator) {
	this.broadcastKommunikator = broadcastKommunilator;
}
public ArrayList<Ladung> getLadungsverzeichnis() {
	return this.ladungsverzeichnis;
}
public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
	this.ladungsverzeichnis = ladungsverzeichnis;
}
/**Kostruktor f�r Raumschiffe mit sieben Attributen*/
public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent,
		int lebenserhaltungssystemInProzent, int androidenAnzahl, String schiffsname) {
	super();
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	this.energieversorgungInProzent = energieversorgungInProzent;
	this.schildeInProzent = schildeInProzent;
	this.huelleInProzent = huelleInProzent;
	this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
	this.androidenAnzahl = androidenAnzahl;
	this.schiffsname = schiffsname;
	this.ladungsverzeichnis = new ArrayList<Ladung>();
	//this.broadcastKommunikator = new ArrayList<String>();

}
/**Zustandsmeldung �ber ein Raumschiff auf der Konsole ausgeben*/
public void getZustand() {
	String meldung = "Schiffsname: "+this.getSchiffsname() + "\n" +
					"Schilde: " +this.getSchildeInProzent() + "\n" +
					"Huelle: " + this.getHuelleInProzent()+ "\n" +
					"Anzahl Photonentorpedos: " + this.getPhotonentorpedoAnzahl() +"\n" +
					"Anzahl Reperaturdroiden: " + this.getAndroidenAnzahl() +  "\n" ;
	System.out.print(meldung);
	System.out.println("-------ENDE ZUSTANDSMELDUNG-------");
			
}
/*Art und Menge einer Ladung auf der KOnsole ausgeben*/
public void ladungAusgeben() {
	for( Ladung x : this.ladungsverzeichnis) {
		System.out.print(x.getBezeichnung()+"\n");
		System.out.print(x.getMenge()+"\n");
		
	}
}
/**Nachrichten welche auf der Konsole ausgegeben werden sowie im BroadcastKommunikato gespeichet werden.(Hilfsfunktion)*/
public void anAlle(String Nachricht) {
	//ich enschlie�e mich dazu die Nachrichten auf die Konsole zu schreiben
	System.out.println("--"+Nachricht);
	broadcastKommunikator.add(Nachricht);
}
/**Schie�t Torpedos wenn vorhanden sonst click-Nachricht*/
public void photonentorpedoSchie�en(Raumschiff ziel) {
	if(getPhotonentorpedoAnzahl() == 0) {
		broadcastKommunikator.add("-=*Click*=_");
	}
	else {
		setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl()-1);
		this.treffer(ziel);
	}
}
/**Ladungsobjekte in das Ladungsverzeichnis aufnehmen*/
public void addLadung(Ladung ladung) {
	this.ladungsverzeichnis.add(ladung);
}


public void ladungsverzeichnisAufraumen() {
	for(Ladung x : this.ladungsverzeichnis) {
		if (x.getMenge() ==0) {
			this.ladungsverzeichnis.remove(this.ladungsverzeichnis.indexOf(x));
		}
	}
}
/**anpassen von Statuswerten nach Treffern*/
private void treffer(Raumschiff ziel) {
	ziel.setSchildeInProzent(ziel.getSchildeInProzent()-50);
	if(ziel.getSchildeInProzent()<0) {
		ziel.setHuelleInProzent(ziel.getHuelleInProzent()-50);
		ziel.setEnergieversorgungInProzent(ziel.getEnergieversorgungInProzent()-50);
		if(ziel.getHuelleInProzent()<=0) {
		ziel.setLebenserhaltungssystemInProzent(0);
		this.anAlle("Lebenserhaltungssysteme vernichtet");}
	}
}
/**Schiesst Phaser bei Energie groesser 50, sonst click-Nachricht*/
public void phaserkanoneSchiessen(Raumschiff ziel) {
	if(this.getEnergieversorgungInProzent()<50) {
		this.anAlle("-=click=-");
	}
	else {
		this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent()-50);
		this.anAlle("Phaserkanone abgeschossen");
		this.treffer(ziel);}
}
/**ausgeben der Eintr�ge des Broadcast-Kommunikators*/
public void Logbuch() {
	for(String x : broadcastKommunikator) {
		System.out.println(x);
	}
}


}
