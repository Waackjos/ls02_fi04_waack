
public class RaumschiffTest {

	public static void main(String[] args) {
//int photonentorpedoAnzahl,EnergieversorgungInProzent, int schildeInProzent, int huelleInProzent,int lebenserhaltungssystemInProzent, int androidenAnzahl, String schiffsname
		Raumschiff Klingonen = new Raumschiff(0,50,50,100,100,2,"IKS Hegh'ta");
		Ladung Schneckensaft = new Ladung("Ferengi Schneckensaft",200);
		Ladung Schwert = new Ladung ("Bat'leth Klingonen Schwert",200);
		Klingonen.addLadung(Schneckensaft);
		Klingonen.addLadung(Schwert);
		
		Raumschiff Romulaner = new Raumschiff(2,50,100,100,100,2,"IRW Khazara");
		Ladung Schrott = new Ladung("Borg-Schrott",5);
		Ladung Materie = new Ladung("Rote Materie",2);
		Ladung Plasma = new Ladung("Plasma Waffe",50);
		Romulaner.addLadung(Schrott);
		Romulaner.addLadung(Materie);
		Romulaner.addLadung(Plasma);
		
		Raumschiff Vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni'Var");
		Ladung Forschungssonde = new Ladung("Forschungssonde",35);
		Ladung Photonentorpedo = new Ladung("Photonentorpedo",3);
		Vulkanier.addLadung(Forschungssonde);
		Vulkanier.addLadung(Photonentorpedo);
		
	//Methoden
		Klingonen.photonentorpedoSchießen(Romulaner);
		Romulaner.phaserkanoneSchiessen(Klingonen);
		Vulkanier.anAlle("Gewalt ist nicht logisch");
		Klingonen.getZustand();
		Klingonen.ladungAusgeben();
		Klingonen.photonentorpedoSchießen(Romulaner);
		Klingonen.photonentorpedoSchießen(Romulaner);
		Klingonen.getZustand();
		Romulaner.getZustand();
		Vulkanier.getZustand();
		Klingonen.ladungAusgeben();
		Romulaner.ladungAusgeben();
		Vulkanier.ladungAusgeben();
		
	}

}
